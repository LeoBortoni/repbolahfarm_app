from xml.dom.pulldom import parseString
from influxdb import InfluxDBClient
from datetime import datetime, timezone
import GPIO_pins
import dht11
import RPi.GPIO as GPIO
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import numpy as np
import serial
from serial import Serial

def run_app_backend(threshold_temp, threshold_hum):
    data_report = {
                    "measurement":"report_bolah_farm_app",
                    "fields":{"temp":None,
                              "humity":None,
                              "level_rio":None,
                              "level_cisterna":None,
                              "status_irrigation_activated":None,
                              "status_sensor_temp":None,
                              "status_sensor_humity":None,
                              "temp_threshold":threshold_temp,
                              "humity_threshold":threshold_hum,
                              "status_possible_to_irrigate":None,
                              "alert_temperature_high":None
                              },
                    "time":str(datetime.now().astimezone().isoformat("T"))
                    }

    _set_pins_default()
    
    temp, status_sensor_temp = _get_temp()
    print (f"temp = {temp} status = {status_sensor_temp}")
    humity, status_sensor_humity = _get_hum()
    print (f"humity = {humity} status = {status_sensor_humity}")
    level_rio = _get_level_rio()
    print (f"level rio = {level_rio}")
    level_cisterna = _get_level_cisterna()
    print (f"level cisterna = {level_cisterna}")

    status_possible_to_irrigate = False
    if (level_cisterna or level_rio):
        status_possible_to_irrigate = True

    status_irrigation_activated = False
    if (status_possible_to_irrigate and status_sensor_humity):
        if humity is not None:
            if humity < threshold_hum:
                status_irrigation_activated = True

                if (level_rio):
                    _irrigate(rio=True)
                    print ("Irrigate rio")
                elif (level_cisterna):
                    _irrigate(cisterna=True)
                    print ("Irrigate cisterna")

    data_report["fields"]["temp"] = temp
    data_report["fields"]["humity"] = humity
    data_report["fields"]["level_rio"] = level_rio
    data_report["fields"]["level_cisterna"] = level_cisterna
    data_report["fields"]["status_irrigation_activated"] = status_irrigation_activated
    data_report["fields"]["status_sensor_temp"] = status_sensor_temp
    data_report["fields"]["status_sensor_humity"] = status_sensor_humity
    data_report["fields"]["temp_threshold"] = threshold_temp
    data_report["fields"]["humity_threshold"] = threshold_hum
    data_report["fields"]["status_possible_to_irrigate"] = status_possible_to_irrigate

    if temp is not None and (temp > threshold_temp):
        data_report["fields"]["alert_temperature_high"] = True
    else:                                                                                                                                                                                                                                                                                                            
        data_report["fields"]["alert_temperature_high"] = False

    print(data_report)                                                                                                                    
    return data_report

def _set_pins_default():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_pins.relay_rio, GPIO.OUT)
    GPIO.output(GPIO_pins.relay_rio, True)
    GPIO.setup(GPIO_pins.relay_cisterna, GPIO.OUT)
    GPIO.output(GPIO_pins.relay_cisterna, True)
    GPIO.setup(GPIO_pins.level_sensor_rio, GPIO.IN)
    GPIO.setup(GPIO_pins.level_sensor_cisterna, GPIO.IN)

def _irrigate(rio=False, cisterna=False):
    time_irrigation = 1.5

    if (rio):
        GPIO.output(GPIO_pins.relay_rio, False)
        time.sleep(time_irrigation)
        GPIO.output(GPIO_pins.relay_rio, True)
    
    if (cisterna):
        GPIO.output(GPIO_pins.relay_cisterna, False)
        time.sleep(time_irrigation)
        GPIO.output(GPIO_pins.relay_cisterna, True)

def _get_level_rio():
    tries = 10
    counter = 0

    for i in range(tries):
        time.sleep(0.5)
        if GPIO.input(GPIO_pins.level_sensor_rio) == 0:
            counter += 1
    print (f"counter rio = {counter}")
    if counter >= 3:
        return True
    
    return False

def _get_level_cisterna():

    tries = 10
    counter = 0

    for i in range(tries):
        time.sleep(0.5)
        if GPIO.input(GPIO_pins.level_sensor_cisterna) == 0:
            counter += 1
    
    print (f"counter cisterna = {counter}")
    if counter >= 3:
        return True
    
    return False


def _get_temp():
    temp = None
    time_span = 1
    retries = 10

    for i in range(retries):
        try:
            instance = dht11.DHT11(pin = GPIO_pins.temp_sensor)
            result = instance.read()

            if result.is_valid():
                temp = result.temperature
                print (type(temp))
                return temp, True
            else:
                print("Error temp read: %d" % result.error_code)
        except Exception as e:
            print(e)

        time.sleep(time_span)

    return temp, False

def _get_hum():
    hum = None
    time_span = 1
    retries = 10

    for i in range(retries):
        try:
            ser = Serial(GPIO_pins.humity_sensor, 9600, timeout=1)
            ser.reset_input_buffer()
            line=""
            # if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            print (f"LINE:{line}")
            if line != "":
                hum=0.95*float(line)
                ser.close()
                return hum, True
            else:
                print("Error hum got")
        except Exception as e:
            print(e)

        time.sleep(time_span)

    return hum, False

def _send_email(body, subject, email):
    mail_content = body
    #The mail addresses and password
    sender_address = 'repbolahfarm@yahoo.com'
    sender_pass = 'eukeyooxzompwshp'
    receiver_address = email
    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = sender_address
    message['To'] = receiver_address
    message['Subject'] = subject
    #The body and the attachments for the mail
    message.attach(MIMEText(mail_content, 'plain'))
    try:
        #Create SMTP session for sending the mail
        session = smtplib.SMTP('smtp.mail.yahoo.com', 587) #use gmail with port
        session.starttls() #enable security
        session.login(sender_address, sender_pass) #login with mail_id and password
        text = message.as_string()
        session.sendmail(sender_address, receiver_address, text)
        session.quit()
    except Exception as e:
        print (e)
        return -1
    return 1

def check_email(data_report, email):
    if (data_report["fields"]["status_sensor_temp"] == False):
        _send_email("The temperature sensor could not read in the last measurement cicle. If you continue to receive this message, you might consider check the conditions of the sensor, in order to ensure that it is still working.", "Alert malfunction of sensor.", email)

    if (data_report["fields"]["status_sensor_humity"] == False):
        _send_email("The humity sensor could not read in the last measurement cicle. If you continue to receive this message, you might consider check the conditions of the sensor, in order to ensure that it is still working.", "Alert malfunction of sensor.", email)

    if (data_report["fields"]["alert_temperature_high"] == True):
        _send_email("The temperature of your farm is above the threshold set acceptable. You migh consider doing something.", "Alert High temperature detected.", email)

    if (data_report["fields"]["status_possible_to_irrigate"] == False):
        _send_email("You are out of water in your reserves. It is not possible to irrigate.", "Alert out of water in the tanks and river", email)

def push_point_database(point, email):
    try:
        client = InfluxDBClient(host="localhost", port=8086)
        client.create_database("test")
        client.switch_database("test")

        client.write_points(point)
        client.close()
    except Exception as e:
        print (e)
        _send_email("Something went wrong with the pushing of data into database. You might consider either restart the application, or call the creators of the software.", "Alert database malfunction", email)

def send_report(email, threshold_temp, threshold_hum, time_report):

    try:
        client = InfluxDBClient(host="localhost", port=8086)
        client.create_database("test")
        client.switch_database("test")

        result = client.query(f'SELECT * FROM "autogen"."report_bolah_farm_app" WHERE time < now() AND time >= now() - {time_report}h')
        series = result.raw['series']

        if len(series) == 0:
            _send_email("You've probably chosen a time report that is lesser than the period check. You are receiving this message because the progrm tried to process the report but no data has been found regarding the last report period.", "Alert could'nt generate report", email)
            return

        columns = series[0]['columns']
        values = np.array(series[0]['values'],dtype=object)

        data_dict = {}

        for i,item in enumerate(columns):
            data_dict[item] = values[:,i]
        
        report = "This is your periodic report of your irrigation system:\n\n"

        # Status sensors
        times_total = len(data_dict["time"])
        report += f"The system made {times_total} routines in the last {time_report} hours.\n"

        times_temp_sensor = np.count_nonzero(data_dict["status_sensor_temp"])
        times_humity_sensor = np.count_nonzero(data_dict["status_sensor_humity"])

        report += f"The temperature sensors worked {times_temp_sensor}/{times_total} times. \n"
        report += f"The humity sensors worked {times_humity_sensor}/{times_total} times. \n"

        # Temperature:
        report += "\nTemperature:\n"

        data_dict["temp"] = data_dict["temp"][data_dict["temp"] != np.array(None)]
        data_dict["temp"] = data_dict["temp"].astype(np.float)

        temp_mean = np.mean(data_dict["temp"])
        temp_max = np.max(data_dict["temp"])
        temp_min = np.min(data_dict["temp"])

        report += f"Average temperature: {round(temp_mean,2)}\n"
        report += f"Maximum temperature: {temp_max}\n"
        report += f"Minimum temperature: {temp_min}\n"

        if temp_mean >= threshold_temp:
            report += f"The average value of temperature is above the threshold set, namely: {threshold_temp}.\n"
        else:
            report += f"The average value of temperature is below the threshold set, namely: {threshold_temp}.\n"

        # Humity
        report += "\nHumity:\n"

        data_dict["humity"] = data_dict["humity"][data_dict["humity"] != np.array(None)]
        data_dict["humity"] = data_dict["humity"].astype(np.float)

        humity_mean = np.mean(data_dict["humity"])
        humity_max = np.max(data_dict["humity"])
        humity_min = np.min(data_dict["humity"])

        report += f"Average humity: {round(humity_mean,2)}\n"
        report += f"Maximum humity: {humity_max}\n"
        report += f"Minimum humity: {humity_min}\n"

        if temp_mean >= threshold_temp:
            report += f"The average value of humity is above the threshold set, namely: {threshold_hum}.\n"
        else:
            report += f"The average value of humity is below the threshold set, namely: {threshold_hum}.\n"

        # Irrigation
        report += "\nIrrigation frequency:\n"

        times_irrigation = np.count_nonzero(data_dict["status_irrigation_activated"])
        report += f"The system has been irrigated {times_irrigation} times.\n"
        
        # Level sensors
        report += "\nWater level:\n"

        times_level_rio = len(data_dict["level_rio"]) - np.count_nonzero(data_dict["level_rio"])
        times_level_cisterna = len(data_dict["level_cisterna"]) - np.count_nonzero(data_dict["level_cisterna"])

        report += f"The river went out of water {times_level_rio} times.\n"
        report += f"The tank went out of water {times_level_cisterna} times.\n"

        print (report)

        _send_email(report, "Periodic report", email)
    except Exception as e:
        print (f"error sending report {e}")
        _send_email("Something went wrong with the querying of data from database. You might consider either restart the application, or call the creators of the software.", "Alert database malfunction", email)