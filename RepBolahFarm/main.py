from re import T
import PyQt5
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QMainWindow,QGraphicsView,QDialog  
from PyQt5 import QtGui
from backend import run_app_backend, check_email, send_report, push_point_database, _send_email

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import sys
import pathlib
import json
import threading
import time

__dir_path__ = str(pathlib.Path(__file__).resolve().parent)

class Commands_json:

    def __init__(self, file):

        self.file = file

        with open(self.file) as json_file:
            self.data = json.load(json_file)

        self.email = None
        self.temp_threshold = None
        self.humity_threshold = None
        self.check_period_seconds = None
        self.report_period_hours  = None

        self.email = self.data["email"]
        self.temp_threshold = self.data["threshold_temp"]
        self.humity_threshold = self.data["threshold_humity"]
        self.check_period_seconds = self.data["check_period_seconds"]
        self.report_period_hours = self.data["report_period_hours"]

    def set_new_config(self, email, temp, humity, ch_period, report_period):
        try:

            em = str(email)
            tp = float(temp)
            hum = float(humity)
            ch = int(float(ch_period))
            report = int(float(report_period))

            self.data["email"] = em
            self.data["threshold_temp"] = tp
            self.data["threshold_humity"] = hum
            self.data["check_period_seconds"] = ch
            self.data["report_period_hours"] = report

            j = json.dumps(self.data, indent=5)
            f = open(self.file,"w")
            print (j,file=f)
            f.close()

            return [em,tp,hum,ch,report]

        except Exception as e:
            print (e)
            return [-1]

def prompt_msg(icon,text,info_text,window_title):
    msg = QtWidgets.QMessageBox()

    if icon=="critical":msg.setIcon(QtWidgets.QMessageBox.Critical)
    elif icon=="question":msg.setIcon(QtWidgets.QMessageBox.Question)
    elif icon=="warning":msg.setIcon(QtWidgets.QMessageBox.Warning)
    elif icon=="info":msg.setIcon(QtWidgets.QMessageBox.Information)

    msg.setText(text)
    msg.setInformativeText(info_text)
    msg.setWindowTitle(window_title)
    msg.exec_()

class MainWindow(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.configs = Commands_json(__dir_path__ + "/config/commands.json")
        self.thread_running_app = None
        self.flag_thread_actived = False
        self.flag_email_not_correct = False

        self.setWindowTitle("RepBolahFarm")
        self.setFixedSize(500,400)
        self.g = QGraphicsView(self)
        self.g.setGeometry(QRect(5,5,490,390))
        self.g.setStyleSheet("background-color: rgb(200, 200, 200)")

        self.font_bold = QtGui.QFont()
        self.font_bold.setBold(True)

        # text email
        self.label_email = QtWidgets.QLabel(self, text="E-mail")
        self.label_email.setGeometry(QRect(20,20,100,20))
        self.label_email.setFont(self.font_bold)
        self.text_email = QtWidgets.QTextEdit(self)
        self.text_email.setGeometry(QRect(20,40,256,50))
        self.text_email.setText(self.configs.email)

        # text set temp threshold
        self.label_temp_threshold = QtWidgets.QLabel(self, text="Temperature threshold")
        self.label_temp_threshold.setGeometry(QRect(20,100,200,20))
        self.label_temp_threshold.setFont(self.font_bold)
        self.text_temp_threshold = QtWidgets.QTextEdit(self)
        self.text_temp_threshold.setGeometry(QRect(20,120,256,35))
        self.text_temp_threshold.setText(str(self.configs.temp_threshold))

        # text set humity threshold
        self.label_humity_threshold = QtWidgets.QLabel(self, text="Humity threshold")
        self.label_humity_threshold.setGeometry(QRect(20,165,200,20))
        self.label_humity_threshold.setFont(self.font_bold)
        self.text_humity_threshold = QtWidgets.QTextEdit(self)
        self.text_humity_threshold.setGeometry(QRect(20,185,256,35))
        self.text_humity_threshold.setText(str(self.configs.humity_threshold))

        # text set check period seconds
        self.label_check_period_seconds = QtWidgets.QLabel(self, text="Cheking period (seconds)")
        self.label_check_period_seconds.setGeometry(QRect(20,230,200,20))
        self.label_check_period_seconds.setFont(self.font_bold)
        self.text_check_period_seconds = QtWidgets.QTextEdit(self)
        self.text_check_period_seconds.setGeometry(QRect(20,250,256,35))
        self.text_check_period_seconds.setText(str(self.configs.check_period_seconds))

        # text set report period hours
        self.label_report_period_hours = QtWidgets.QLabel(self, text="Report period (hours)")
        self.label_report_period_hours.setGeometry(QRect(20,295,200,20))
        self.label_report_period_hours.setFont(self.font_bold)
        self.text_report_period_hours = QtWidgets.QTextEdit(self)
        self.text_report_period_hours.setGeometry(QRect(20,315,256,35))
        self.text_report_period_hours.setText(str(self.configs.report_period_hours))

        # Button start/stop
        self.button_start_stop = QtWidgets.QPushButton(self, text="Start")
        self.button_start_stop.setGeometry(QRect(340,190,90,35))
        self.button_start_stop.clicked.connect(self._button_start_stop_callback)

    def test_email_valid(self, email):
        return _send_email("This e-mail address has been connected to the RepBolahFarm app.",'Registration in RepBolahFarm app.',email)

    def _run_app_backend(self, args):

        time_end_check = 0.0
        time_end_report = time.time() + args[4]*60*60

        while (self.flag_thread_actived == True):
            time_now = time.time()

            if (time_now > time_end_check):
                data_report = run_app_backend(args[1],args[2])
                check_email(data_report, args[0])
                push_point_database([data_report], args[0])
                time_end_check = time.time() + args[3]

            if (time_now > time_end_report):
                send_report(args[0],args[1],args[2],args[4])
                time_end_report = time.time() + args[4]*60*60
            
            time.sleep(0.01)

        print ("Thread ended")

    def __checkpassword(self, password):
        if password == "entorpezecuras":
            return True
        else:
            return False

    def __inlogin(self, password, dialog):
            if (self.__checkpassword(password) == True):
                if self.button_start_stop.text() == "Start":

                    # do things
                    args = self.configs.set_new_config(
                                                        self.text_email.toPlainText(),
                                                        self.text_temp_threshold.toPlainText(),
                                                        self.text_humity_threshold.toPlainText(),
                                                        self.text_check_period_seconds.toPlainText(),
                                                        self.text_report_period_hours.toPlainText()
                                                        )
                    list_size = len(args)

                    if list_size == 1:
                        prompt_msg(
                                    "critical",
                                    "\nYou typed wrong arguments. Check whether the fields' texts matches the expectations described in 'help' section. The program will not move forward.",
                                    "Wrong type of varible detected in inputs.",
                                    "Inputs error"
                                    )
                    elif list_size == 5:
                        prompt_msg(
                                    "info",
                                    f"\nThe application will move forward with the following arguments:\n E-mail: {args[0]}\n Temperature: {args[1]} Celsius \n Humity: {args[2]} % \n Checking period: {args[3]} s \n Report period: {args[4]} hr",
                                    "The application will be put to run.",
                                    "Information"
                                    )
                        #TODO uncomment this
                        if (self.test_email_valid(args[0]) < 0):
                            prompt_msg(
                                "warning",
                                "\nPlease check whether the e-mail passed indeed does exist. Some thing went wrong with the validation of the e-mail address provided.",
                                "Could not connect to the e-mail provided.",
                                "Inputs error"
                                )
                            return

                        self.flag_thread_actived = True
                        self.thread_running_app = threading.Thread(target=self._run_app_backend,args=(args,),daemon=True)
                        self.thread_running_app.start()

                        self.button_start_stop.setText("Stop")
                else:
                    self.flag_thread_actived = False
                    self.thread_running_app.join()
                    self.button_start_stop.setText("Start")
            else:
                prompt_msg(
                            "warning",
                            f"You've set the wrong password. Contact someone who is allowed to config this application, because you are not.",
                            "Configs have not been set.",
                            "Wrong password."
                            )
            dialog.close()

    def _button_start_stop_callback(self):
        login_dialog = QDialog(self)
        login_dialog.setWindowTitle("Login")
        login_dialog.setFixedSize(200, 200)

        login_label = QtWidgets.QLabel(login_dialog, text="Provide the password:")
        login_label.setGeometry(QRect(10,30,180,30))

        login_text = QtWidgets.QLineEdit(login_dialog)
        login_text.setEchoMode(QtWidgets.QLineEdit.Password)
        login_text.setGeometry(QRect(10,100,180,30))

        nodes_button_done = QtWidgets.QPushButton(login_dialog, text="login")
        nodes_button_done.setGeometry(QRect(75,150,50,30))
        nodes_button_done.clicked.connect(lambda: self.__inlogin(login_text.text(), login_dialog))

        login_dialog.exec_()


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    mainwindow = MainWindow()
    mainwindow.show()

    sys.exit(app.exec_())
