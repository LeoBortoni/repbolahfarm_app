from influxdb import InfluxDBClient
from datetime import datetime
import random

if __name__ == "__main__":
    
    client = InfluxDBClient(host="localhost", port=8086)
    client.create_database("test")
    client.switch_database("test")
    point = [{
                "measurement":"temperature",
                "fields":{"temp":random.randint(20,30)},
                "time":str(datetime.now().isoformat("T"))
            }]
    client.write_points(point)