import RPi.GPIO as GPIO
import dht11
import time

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()

# read data using pin 14
# instance = dht11.DHT11(pin = 27)
# result = instance.read()

# if result.is_valid():
#     print("Temperature: %-3.1f C" % result.temperature)
#     print("Humidity: %-3.1f %%" % result.humidity)
# else:
#     print("Error: %d" % result.error_code)

hum = None
time_span = 1
retries = 60

for i in range(retries):
    try:
        instance = dht11.DHT11(pin = 27)
        result = instance.read()

        if result.is_valid():
            hum = result.humidity
            print (hum)
        else:
            print("Error temp read: %d" % result.error_code)
    except Exception as e:
        print(e)

    time.sleep(time_span)