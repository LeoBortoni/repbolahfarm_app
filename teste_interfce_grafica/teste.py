import PyQt5

from PyQt5 import QtWidgets,QtCore
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QMainWindow,QGraphicsView
import sys

class MainWindow(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)

        self.setWindowTitle("Controlador de parâmetros")
        self.setFixedSize(800,600)
        self.g = QGraphicsView(self)
        self.g.setGeometry(QRect(5,240,480,355))
        self.g.setStyleSheet("background-color: rgb(223, 223, 223)")


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)
    mainwindow = MainWindow()
    mainwindow.show()
    sys.exit(app.exec_())
