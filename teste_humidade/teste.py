#!/usr/bin/env python3
import serial
from serial import Serial
if __name__ == '__main__':
    ser = Serial('/dev/ttyUSB0', 9600, timeout=1)
    ser.reset_input_buffer()
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            print(line)
            print(float(line))