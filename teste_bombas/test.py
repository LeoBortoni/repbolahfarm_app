import RPi.GPIO as GPIO
import time

def print_1(channel):
    print ("1")

def print_2(channel):
    print ("2")

GPIO.setmode(GPIO.BCM)
bomba_rio = 26
bomba_cisterna = 19

GPIO.setup(bomba_rio, GPIO.IN)
GPIO.setup(bomba_cisterna, GPIO.IN)

# GPIO.add_event_detect(bomba_rio, GPIO.FALLING, callback=print_1)
# GPIO.add_event_detect(bomba_cisterna, GPIO.FALLING, callback=print_2)


print ("bomba rio")
print (GPIO.input(bomba_rio))

print ("bomba cisterna")
print (GPIO.input(bomba_cisterna))

time.sleep(0.25)