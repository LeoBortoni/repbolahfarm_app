import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
rele1 = 21
rele2 = 20
state = False
GPIO.setup(rele1, GPIO.OUT)
GPIO.setup(rele2, GPIO.OUT)
GPIO.output(rele1, False)
GPIO.output(rele2, False)
time.sleep(2)
GPIO.output(rele1, True)
GPIO.output(rele2, True)